package hu.raiffeisen.rest.service;

import hu.raiffeisen.rest.model.LoginDTO;
import hu.raiffeisen.rest.model.LoginResponseDTO;
import hu.raiffeisen.service.LoginService;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/login")
public class LoginRestService {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginDTO data) {
        LoginService loginService = new LoginService();
        LoginResponseDTO res = loginService.login(data);
        return Response.status(201).entity(res).build();
    }
}
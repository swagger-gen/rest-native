package hu.raiffeisen.rest.service;


import hu.raiffeisen.rest.model.UserDTO;
import hu.raiffeisen.service.UserService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/users")
public class UsersRestService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UserDTO[] getDefaultUserInJSON() {
        UserService userService = new UserService();
        return userService.getUsers();
    }
}

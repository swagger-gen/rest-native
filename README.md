# Kézzel összerakott resteasy alkalmazás

Előnyök:
* minimális függőség a pom-ban
* csak a szükséges mennyiségű kód

Hátrányok:
* DTO osztályokat kézzel kell létrehozni